resource "aws_s3_bucket" "terraform_state" {
  region = "${var.region}"
  bucket = "${var.bucket_name}"

  versioning {
    enabled = true
  }

  tags {
    state_path = "${var.tag-state_path}"
    tf_module  = "${var.tag-tf_module}"
  }
}
