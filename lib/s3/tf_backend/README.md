# tf_backend_s3

Terraform supports locking when using shared state. The S3 backend requires a bucket and a DynamoDB table. [S3 backend documentation](https://www.terraform.io/docs/backends/types/s3.html)

This module implements the locking scheme. Its state should be stored localy (in version control) since there's no remote backend set up yet. Make sure to pull the latest code if the state is stored in version control.

No encryption is implemented for the S3 bucket. This can be a good improvement.
