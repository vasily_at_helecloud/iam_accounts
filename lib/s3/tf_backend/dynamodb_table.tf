resource "aws_dynamodb_table" "terraform_lock" {
  name           = "terraform-lock"
  hash_key       = "LockID"
  read_capacity  = 1
  write_capacity = 1

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    state_path = "${var.tag-state_path}"
    tf_module  = "${var.tag-tf_module}"
  }
}
