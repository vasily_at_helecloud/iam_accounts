data "template_file" "account_role_policy" {
  template = "${file("${path.module}/templates/account_role_policy.json")}"

  vars {
    account_id = "${var.account_id}"
    role_name  = "${var.role_name}"
  }
}

resource "aws_iam_policy" "account_role_policy" {
  name   = "${var.policy_name}"
  policy = "${data.template_file.account_role_policy.rendered}"
}
