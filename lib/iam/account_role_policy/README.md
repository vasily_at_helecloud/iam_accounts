# account_role_policy

Creates a policy which allows __one__ role to be assumed given the user has logged using MFA.
