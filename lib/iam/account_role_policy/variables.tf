variable "account_id" {}
variable "policy_name" {}

variable "role_name" {
  default = "HeleCloud-Admin"
}

variable "region" {
  default = "eu-west-1"
}
