variable "user_name" {}
variable "sbx_id" {}

variable "groups" {
  type = "list"
}

variable "region" {
  default = "eu-west-1"
}
