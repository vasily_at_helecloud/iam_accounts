module "sandbox" {
  source      = "../account_role_policy"
  account_id  = "${var.sbx_id}"
  policy_name = "${var.user_name}-${var.sbx_id}"
}

resource "aws_iam_user" "user" {
  name = "${var.user_name}"
}

resource "aws_iam_user_policy_attachment" "sandbox" {
  user       = "${var.user_name}"
  policy_arn = "${module.sandbox.policy}"
}

resource "aws_iam_user_group_membership" "groups" {
  user = "${var.user_name}"

  groups = [
    "${var.groups}",
  ]
}
