# user_with_sandbox

Manages an IAM user.

* Uses the `account_role_policy` module to create a policy `<iam_user>-<sbx_account_id>` that will grant the user access to the sandbox admin role.
* Adds the user to each group of the provided list.
