module "HeleCloud-Classroom0_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "088833130955"
  policy_name = "HeleCloud-Classroom0_Admin"
  role_name   = "CrossAccountAdminAccess-f-CrossAccountAccessAdminR-1B2VESYQKO1RJ"
}

module "HeleCloud-Classroom1_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "632041470966"
  policy_name = "HeleCloud-Classroom1_Admin"
  role_name   = "HeleCloud_Administrative_Role"
}

module "HC-Infra_Security_Manage_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "891584517969"
  policy_name = "HC-Infra_Security_Manage_Admin"
  role_name   = "InfraSecurityManage"
}

resource "aws_iam_group" "HC_Internal" {
  name = "HC_Internal"
}

resource "aws_iam_group_policy_attachment" "HeleCloud-Classroom0_Admin_HC_Internal" {
  group      = "${aws_iam_group.HC_Internal.name}"
  policy_arn = "${module.HeleCloud-Classroom0_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "HeleCloud-Classroom1_Admin_HC_Internal" {
  group      = "${aws_iam_group.HC_Internal.name}"
  policy_arn = "${module.HeleCloud-Classroom1_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "HC-Infra_Security_Manage_Admin_HC_Internal" {
  group      = "${aws_iam_group.HC_Internal.name}"
  policy_arn = "${module.HC-Infra_Security_Manage_Admin.policy}"
}
