module "Assystem-SBX_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "057235298679"
  policy_name = "Assystem-SBX_Admin"
}

resource "aws_iam_group" "Assystem_Admin" {
  name = "Assystem_Admin"
}

resource "aws_iam_group_policy_attachment" "Assystem-SBX_Admin_Assystem_Admin" {
  group      = "${aws_iam_group.Assystem_Admin.name}"
  policy_arn = "${module.Assystem-SBX_Admin.policy}"
}
