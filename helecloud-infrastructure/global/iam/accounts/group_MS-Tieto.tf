resource "aws_iam_group" "MS-Tieto" {
  name = "MS-Tieto"
}

resource "aws_iam_group_policy_attachment" "Tieto-Posti-SOC_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.Tieto-Posti-SOC_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Tieto-Posti-Prod_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.Tieto-Posti-Prod_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Tieto-SOC_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.Tieto-SOC_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Tieto-Identity_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.Tieto-Identity_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSGCL-Legal_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.AWS-Tieto-ZSGCL-Legal_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-TSMIDUICSFIN-Architects_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.AWS-Tieto-TSMIDUICSFIN-Architects_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSPI-ICS_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.AWS-Tieto-ZSPI-ICS_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSPI-IS-NCOC-UP_Admin-MS-Tieto" {
  group      = "${aws_iam_group.MS-Tieto.name}"
  policy_arn = "${module.AWS-Tieto-ZSPI-IS-NCOC-UP_Admin.policy}"
}
