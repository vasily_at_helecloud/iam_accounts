module "CC3-Development_Audit" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "939326250094"
  policy_name = "YouView-Development_Audit"
  role_name   = "cc3-helecloud-itc-security-audit"
}

module "CC4-Production_Audit" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "448041004003"
  policy_name = "YouView-Production_Audit"
  role_name   = "cc4-helecloud-itc-security-audit"
}

module "CC5-Ops_Audit" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "996631381108"
  policy_name = "YouView-Ops_Audit"
  role_name   = "cc5-helecloud-itc-security-audit"
}

resource "aws_iam_group" "YouView_Admin" {
  name = "YouView_Admin"
}

resource "aws_iam_group_policy_attachment" "CC3-Development_Audit_YouView_Admin" {
  group      = "${aws_iam_group.YouView_Admin.name}"
  policy_arn = "${module.CC3-Development_Audit.policy}"
}

resource "aws_iam_group_policy_attachment" "CC4-Production_Audit_YouView_Admin" {
  group      = "${aws_iam_group.YouView_Admin.name}"
  policy_arn = "${module.CC4-Production_Audit.policy}"
}

resource "aws_iam_group_policy_attachment" "CC5-Ops_Audit_YouView_Admin" {
  group      = "${aws_iam_group.YouView_Admin.name}"
  policy_arn = "${module.CC5-Ops_Audit.policy}"
}
