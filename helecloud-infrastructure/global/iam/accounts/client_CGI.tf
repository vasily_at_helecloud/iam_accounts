module "CGI-SIT_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "074687639395"
  policy_name = "CGI-SIT_Admin"
  role_name   = "Helecloud-Admin"
}

module "CGI-SOC_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "091911872322"
  policy_name = "CGI-SOC_Admin"
  role_name   = "Helecloud-Admin"
}

resource "aws_iam_group" "CGI_Admin" {
  name = "CGI_Admin"
}

resource "aws_iam_group_policy_attachment" "CGI-SIT_Admin_CGI_Admin" {
  group      = "${aws_iam_group.CGI_Admin.name}"
  policy_arn = "${module.CGI-SIT_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "CGI-SOC_Admin_CGI_Admin" {
  group      = "${aws_iam_group.CGI_Admin.name}"
  policy_arn = "${module.CGI-SOC_Admin.policy}"
}
