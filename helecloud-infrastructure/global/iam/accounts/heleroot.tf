module "HeleRoot_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "535169500385"
  policy_name = "HeleRoot_Admin"
}

resource "aws_iam_group_policy_attachment" "HeleRoot_Admin_HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "${module.HeleRoot_Admin.policy}"
}
