module "Tieto-Posti-SOC_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "006177185466"
  policy_name = "Tieto-Posti-SOC_Admin"
}

module "Tieto-Posti-Prod_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "328989877745"
  policy_name = "Tieto-Posti-Prod_Admin"
}

resource "aws_iam_group" "Tieto-Posti_Admin" {
  name = "Tieto-Posti_Admin"
}

resource "aws_iam_group_policy_attachment" "Tieto-Posti-SOC_Admin_Tieto-Posti_Admin" {
  group      = "${aws_iam_group.Tieto-Posti_Admin.name}"
  policy_arn = "${module.Tieto-Posti-SOC_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Tieto-Posti-Prod_Admin_Tieto-Posti_Admin" {
  group      = "${aws_iam_group.Tieto-Posti_Admin.name}"
  policy_arn = "${module.Tieto-Posti-Prod_Admin.policy}"
}
