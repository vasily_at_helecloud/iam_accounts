resource "aws_iam_group" "MS-Non_Tieto-2" {
  name = "MS-Non_Tieto-2"
}

resource "aws_iam_group_policy_attachment" "Three-Cisco_Admin-MS-Non_Tieto-2" {
  group      = "${aws_iam_group.MS-Non_Tieto-2.name}"
  policy_arn = "${module.Three-Cisco_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Three_Admin-MS-Non_Tieto-2" {
  group      = "${aws_iam_group.MS-Non_Tieto-2.name}"
  policy_arn = "${module.Three_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Three-Mashery_Admin-MS-Non_Tieto-2" {
  group      = "${aws_iam_group.MS-Non_Tieto-2.name}"
  policy_arn = "${module.Three-Mashery_Admin.policy}"
}
