terraform {
  required_version = "= 0.11.8"

  backend "s3" {
    region         = "eu-west-1"
    bucket         = "helecloud-infrastructure-terraform-state"
    key            = "global/iam/accounts.tfstate"
    dynamodb_table = "terraform-lock"
  }
}

provider "aws" {
  region = "eu-west-1"
}
