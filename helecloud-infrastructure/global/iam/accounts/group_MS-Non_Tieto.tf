resource "aws_iam_group" "MS-Non_Tieto" {
  name = "MS-Non_Tieto"
}

resource "aws_iam_group_policy_attachment" "Assystem-SBX_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.Assystem-SBX_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "BRB-PROD_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.BRB-PROD_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "BRB-SBX_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.BRB-SBX_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "BRB-SOC_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.BRB-SOC_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "CGI-SIT_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.CGI-SIT_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "CGI-SOC_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.CGI-SOC_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Finastra-Root_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.Finastra-Root_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Finastra-Live_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.Finastra-Live_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Finastra-SBX_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.Finastra-SBX_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "NetSend_Admin-MS-Non_Tieto" {
  group      = "${aws_iam_group.MS-Non_Tieto.name}"
  policy_arn = "${module.NetSend_Admin.policy}"
}
