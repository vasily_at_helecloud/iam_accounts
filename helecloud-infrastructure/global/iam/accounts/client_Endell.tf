module "Endell-dev_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "495713461397"
  policy_name = "Endell-dev_Admin"
  role_name   = "helecloud"
}

module "Endell-prod_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "516500117142"
  policy_name = "Endell-prod_Admin"
  role_name   = "helecloud"
}

module "Endell-dev_Audit" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "495713461397"
  policy_name = "Endell-dev_Audit"
  role_name   = "dev-helecloud-audit-access"
}

module "Endell-prod_Audit" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "516500117142"
  policy_name = "Endell-prod_Audit"
  role_name   = "prod-helecloud-audit-access"
}

resource "aws_iam_group" "Endell_Admin" {
  name = "Endell_Admin"
}

resource "aws_iam_group_policy_attachment" "Endell-dev_Admin_Endell_Admin" {
  group      = "${aws_iam_group.Endell_Admin.name}"
  policy_arn = "${module.Endell-dev_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Endell-prod_Admin_Endell_Admin" {
  group      = "${aws_iam_group.Endell_Admin.name}"
  policy_arn = "${module.Endell-prod_Admin.policy}"
}

resource "aws_iam_group" "Endell_Audit" {
  name = "Endell_Audit"
}

resource "aws_iam_group_policy_attachment" "Endell-dev_Audit_Endell_Audit" {
  group      = "${aws_iam_group.Endell_Audit.name}"
  policy_arn = "${module.Endell-dev_Audit.policy}"
}

resource "aws_iam_group_policy_attachment" "Endell-prod_Audit_Endell_Audit" {
  group      = "${aws_iam_group.Endell_Audit.name}"
  policy_arn = "${module.Endell-prod_Audit.policy}"
}
