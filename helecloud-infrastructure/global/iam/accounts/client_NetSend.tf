module "NetSend_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "634600782720"
  policy_name = "NetSend_Admin"
}

resource "aws_iam_group" "NetSend_Admin" {
  name = "NetSend_Admin"
}

resource "aws_iam_group_policy_attachment" "NetSend_Admin_NetSend_Admin" {
  group      = "${aws_iam_group.NetSend_Admin.name}"
  policy_arn = "${module.NetSend_Admin.policy}"
}
