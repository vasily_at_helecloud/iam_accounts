# accounts

This code manages IAM users, groups, policies and roles for the `helecloud-infrastructure` account.

To continue being useful, all manual changes should also be implemented here __and__ imported. Each type of resource is imported differently. For example [iam_user](https://www.terraform.io/docs/providers/aws/r/iam_user.html#import). Refer to the Terraform documentation about [importing](https://www.terraform.io/docs/import/usage.html).

The access to any other account happens by assuming a role in that account. The role must have a trust relationship which allows to be assumed by a member of the `helecloud-infrastructure` account. 

Management of accounts other than `helecloud-infrastructure` and their roles is not part of this module.

It takes some gymnastics to achieve a useful arrangements that fits into the [limitations imposed by AWS](https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_iam-limits.html), like:

* maximum of 10 groups per user
* maximum of 10 policies per group

## client_*.tf

They implement the policies to connect to each account for a given client. The policies that grant similar type of access (admin/audit) are combined into one or more groups.

Users become members of the groups. Client account policies are not attached directly to the user.

## group_*.tf

These implement convenience groups to avoid adding too many groups to each user.

## users.tf

It manages all IAM users. The ones which have a sandbox can use the `lib/iam/user_with_sandbox` module to implement everything as a package. The users without a sandbox account are described at the bottom by simply using the Terraform resource types.

The sandbox policies are directly attached to users since they are very likely to have only one or two users.

## helecloud_infrastructure.tf

Implements structure and permissions internal to the account and doesn't concern with access to external accounts:

### groups

* `HI_Admins` with attached policies:

    - `ManageOwnMFA`

        > users can set up and manage their own MFA

    - `HI_Admin`

        > a permission to assume role `HeleCloud-Admin`

    - `IAMUserChangePassword`

        > users can change their own passwords

    - `ManageOwnAccessKeys` 

        > users can manage their own Access Keys

    - `IAMReadOnlyAccess`

        > users can view IAM resources to find own user and attributes

    - `IAMUserSSHKeys`

        > users can manage own SSH Keys

* `HI_Users` with attached policies:

    - `ManageOwnMFA`

    - `HI_ReadOnly`

        > a permission to assume role `HeleCloud-ReadOnly`

    - `IAMUserChangePassword`

    - `ManageOwnAccessKeys`

    - `IAMReadOnlyAccess`

    - `IAMUserSSHKeys`
