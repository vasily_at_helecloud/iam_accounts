module "Traiana-Lab_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "655055017009"
  policy_name = "Traiana-Lab_Admin"
  role_name   = "HeleCloud-Role"
}

module "Traiana-UAT_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "597739506299"
  policy_name = "Traiana-UAT_Admin"
  role_name   = "HeleCloud-Role"
}

module "Traiana-Sandbox_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "997846170492"
  policy_name = "Traiana-Sandbox_Admin"
  role_name   = "HeleCloud-Role"
}

module "Traiana-Services_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "550959330073"
  policy_name = "Traiana-Services_Admin"
  role_name   = "HeleCloud-Role"
}

module "Infinity-lab_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "597946778597"
  policy_name = "Infinity-lab_Admin"
  role_name   = "HeleCloud-Role"
}

module "Infinity-uat_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "779812939232"
  policy_name = "Infinity-uat_Admin"
}

module "Infinity-prod_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "649357889389"
  policy_name = "Infinity-prod_Admin"
}

module "Infinity-preprod_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "588515501569"
  policy_name = "Infinity-preprod_Admin"
  role_name   = "HeleCloud-Role"
}

module "Infinitiy-soc_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "851738176027"
  policy_name = "Infinitiy-soc_Admin"
  role_name   = "HeleCloud-Role"
}

resource "aws_iam_group" "Traiana_Admin" {
  name = "Traiana_Admin"
}

resource "aws_iam_group_policy_attachment" "Traiana-Lab_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Traiana-Lab_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Traiana-UAT_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Traiana-UAT_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Traiana-Sandbox_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Traiana-Sandbox_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Traiana-Services_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Traiana-Services_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Infinity-lab_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Infinity-lab_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Infinity-uat_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Infinity-uat_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Infinity-prod_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Infinity-prod_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Infinity-preprod_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Infinity-preprod_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Infinitiy-soc_Admin_Traiana_Admin" {
  group      = "${aws_iam_group.Traiana_Admin.name}"
  policy_arn = "${module.Infinitiy-soc_Admin.policy}"
}
