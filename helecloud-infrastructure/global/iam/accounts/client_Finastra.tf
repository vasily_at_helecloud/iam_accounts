module "Finastra-Root_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "968795543721"
  policy_name = "Finastra-Root_Admin"
}

module "Finastra-Live_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "291525339654"
  policy_name = "Finastra-Live_Admin"
}

module "Finastra-SBX_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "156295266914"
  policy_name = "Finastra-SBX_Admin"
}

resource "aws_iam_group" "Finastra_Admin" {
  name = "Finastra_Admin"
}

resource "aws_iam_group_policy_attachment" "Finastra-Root_Admin_Finastra_Admin" {
  group      = "${aws_iam_group.Finastra_Admin.name}"
  policy_arn = "${module.Finastra-Root_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Finastra-Live_Admin_Finastra_Admin" {
  group      = "${aws_iam_group.Finastra_Admin.name}"
  policy_arn = "${module.Finastra-Live_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Finastra-SBX_Admin_Finastra_Admin" {
  group      = "${aws_iam_group.Finastra_Admin.name}"
  policy_arn = "${module.Finastra-SBX_Admin.policy}"
}
