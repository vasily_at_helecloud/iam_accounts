module "Zopa-Global_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "831842855543"
  policy_name = "Zopa-Global_Admin"
  role_name   = "Zopa_Global_Helecloud_AdminAccess"
}

module "Zopa-SBX_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "659222956801"
  policy_name = "Zopa-SBX_Admin"
  role_name   = "Zopa_Sbx_Helecloud_AdminAccess"
}

module "Zopa-Prod_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "894456544055"
  policy_name = "Zopa-Prod_Admin"
  role_name   = "Zopa_Prod_Helecloud_AdminAccess"
}

module "Zopa-Logging_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "508666044109"
  policy_name = "Zopa-Logging_Admin"
  role_name   = "Zopa_Logging_Helecloud_AdminAccess"
}

module "Zopa-Data-Science_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "241567918254"
  policy_name = "Zopa-Data-Science_Admin"
  role_name   = "Zopa_DSl_Helecloud_AdminAccess"
}

resource "aws_iam_group" "Zopa_Admin" {
  name = "Zopa_Admin"
}

resource "aws_iam_group_policy_attachment" "Zopa-Global_Admin_Zopa_Admin" {
  group      = "${aws_iam_group.Zopa_Admin.name}"
  policy_arn = "${module.Zopa-Global_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Zopa-SBX_Admin_Zopa_Admin" {
  group      = "${aws_iam_group.Zopa_Admin.name}"
  policy_arn = "${module.Zopa-SBX_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Zopa-Prod_Admin_Zopa_Admin" {
  group      = "${aws_iam_group.Zopa_Admin.name}"
  policy_arn = "${module.Zopa-Prod_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Zopa-Logging_Admin_Zopa_Admin" {
  group      = "${aws_iam_group.Zopa_Admin.name}"
  policy_arn = "${module.Zopa-Logging_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Zopa-Data-Science_Admin_Zopa_Admin" {
  group      = "${aws_iam_group.Zopa_Admin.name}"
  policy_arn = "${module.Zopa-Data-Science_Admin.policy}"
}
