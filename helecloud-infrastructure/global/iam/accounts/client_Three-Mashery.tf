module "Three-Mashery_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "330937847434"
  policy_name = "Three-Mashery_Admin"
}

resource "aws_iam_group" "Three-Mashery_Admin" {
  name = "Three-Mashery_Admin"
}

resource "aws_iam_group_policy_attachment" "Three-Mashery_Admin_Three-Mashery_Admin" {
  group      = "${aws_iam_group.Three-Mashery_Admin.name}"
  policy_arn = "${module.Three-Mashery_Admin.policy}"
}
