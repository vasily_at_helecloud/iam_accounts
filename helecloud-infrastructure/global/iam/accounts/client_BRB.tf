module "BRB-PROD_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "823646791467"
  policy_name = "BRB-PROD_Admin"
}

module "BRB-SBX_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "748273941538"
  policy_name = "BRB-SBX_Admin"
}

module "BRB-SOC_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "479734317345"
  policy_name = "BRB-SOC_Admin"
}

resource "aws_iam_group" "BRB_Admin" {
  name = "BRB_Admin"
}

resource "aws_iam_group_policy_attachment" "BRB-PROD_Admin_BRB_Admin" {
  group      = "${aws_iam_group.BRB_Admin.name}"
  policy_arn = "${module.BRB-PROD_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "BRB-SBX_Admin_BRB_Admin" {
  group      = "${aws_iam_group.BRB_Admin.name}"
  policy_arn = "${module.BRB-SBX_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "BRB-SOC_Admin_BRB_Admin" {
  group      = "${aws_iam_group.BRB_Admin.name}"
  policy_arn = "${module.BRB-SOC_Admin.policy}"
}
