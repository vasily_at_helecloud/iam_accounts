module "Three-Cisco_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "695674666576"
  policy_name = "Three-Cisco_Admin"
}

module "Three_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "060654072610"
  policy_name = "Three_Admin"
  role_name   = "HeleCloud_Administrative_Role"
}

resource "aws_iam_group" "Three_Admin" {
  name = "Three_Admin"
}

resource "aws_iam_group_policy_attachment" "Three-Cisco_Admin_Three_Admin" {
  group      = "${aws_iam_group.Three_Admin.name}"
  policy_arn = "${module.Three-Cisco_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Three_Admin_Three_Admin" {
  group      = "${aws_iam_group.Three_Admin.name}"
  policy_arn = "${module.Three_Admin.policy}"
}
