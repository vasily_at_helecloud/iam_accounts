module "Tieto-SOC_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "542492908355"
  policy_name = "Tieto-SOC_Admin"
}

module "Tieto-Identity_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "488006143195"
  policy_name = "Tieto-Identity_Admin"
}

module "AWS-Tieto-ZSGCL-Legal_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "291476900773"
  policy_name = "Tieto-ZSGCL-Legal_Admin"
}

module "AWS-Tieto-TSMIDUICSFIN-Architects_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "352833760438"
  policy_name = "Tieto-TSMIDUICSFIN-Architects_Admin"
}

module "AWS-Tieto-ZSPI-ICS_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "772123536587"
  policy_name = "Tieto-ZSPI-ICS_Admin"
}

module "AWS-Tieto-ZSPI-IS-NCOC-UP_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "700990884567"
  policy_name = "Tieto-ZSPI-IS-NCOC-UP_Admin"
}

module "AWS-Tieto-ZSTJ-CCS_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "128562303062"
  policy_name = "AWS-Tieto-ZSTJ-CCS_Admin"
}

resource "aws_iam_group" "Tieto_Admin" {
  name = "Tieto_Admin"
}

resource "aws_iam_group_policy_attachment" "Tieto-SOC_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.Tieto-SOC_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "Tieto-Identity_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.Tieto-Identity_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSGCL-Legal_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.AWS-Tieto-ZSGCL-Legal_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-TSMIDUICSFIN-Architects_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.AWS-Tieto-TSMIDUICSFIN-Architects_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSPI-ICS_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.AWS-Tieto-ZSPI-ICS_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSPI-IS-NCOC-UP_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.AWS-Tieto-ZSPI-IS-NCOC-UP_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "AWS-Tieto-ZSTJ-CCS_Admin_Tieto_Admin" {
  group      = "${aws_iam_group.Tieto_Admin.name}"
  policy_arn = "${module.AWS-Tieto-ZSTJ-CCS_Admin.policy}"
}
