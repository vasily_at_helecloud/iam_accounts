module "aleksandarb" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "aleksandarb"
  sbx_id    = "096342428149"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
  ]
}

module "angelm" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "angelm"
  sbx_id    = "581971120024"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.Finastra_Admin.name}",
    "${aws_iam_group.Tieto_Admin.name}",
    "${aws_iam_group.Tieto-Posti_Admin.name}",
    "${aws_iam_group.Traiana_Admin.name}",
  ]
}

module "atanasc" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "atanasc"
  sbx_id    = "618541241448"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "atanasg" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "atanasg"
  sbx_id    = "743313234077"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "blagovesti" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "blagovesti"
  sbx_id    = "959369198207"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "chavdarr" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "chavdarr"
  sbx_id    = "530913079960"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "danielr" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "danielr"
  sbx_id    = "424709202265"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
    "${aws_iam_group.Traiana_Admin.name}",
  ]
}

module "deepakv" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "deepakv"
  sbx_id    = "393321347560"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
    "${aws_iam_group.HC_Internal.name}",
  ]
}

module "denislavd" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "denislavd"
  sbx_id    = "397899088771"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "denitsav" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "denitsav"
  sbx_id    = "950352281332"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "detelinav" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "detelinav"
  sbx_id    = "786231382606"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "dianad" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "dianad"
  sbx_id    = "628630405288"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "filipd" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "filipd"
  sbx_id    = "062639323510"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "georgik" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "georgik"
  sbx_id    = "952199313468"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
  ]
}

module "georgit" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "georgit"
  sbx_id    = "372047877467"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "hristoforg" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "hristoforg"
  sbx_id    = "318743425885"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
  ]
}

module "iliyana" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "iliyana"
  sbx_id    = "099873477117"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "ivanb" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "ivanb"
  sbx_id    = "058060066421"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "ivayloc" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "ivayloc"
  sbx_id    = "276734431722"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
  ]
}

module "ivaylov" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "ivaylov"
  sbx_id    = "314769001592"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
    "${aws_iam_group.BRB_Admin.name}",
    "${aws_iam_group.Endell_Admin.name}",
    "${aws_iam_group.Endell_Audit.name}",
    "${aws_iam_group.Finastra_Admin.name}",
    "${aws_iam_group.Three-Mashery_Admin.name}",
  ]
}

module "jefft" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "jefft"
  sbx_id    = "087994864603"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
  ]
}

module "jons" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "jons"
  sbx_id    = "645238402036"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
    "${aws_iam_group.Three-Mashery_Admin.name}",
  ]
}

module "kirilk" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "kirilk"
  sbx_id    = "622303554998"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "kostadinl" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "kostadinl"
  sbx_id    = "987551707139"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "lyudmilb" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "lyudmilb"
  sbx_id    = "187384025050"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.Tieto_Admin.name}",
    "${aws_iam_group.Tieto-Posti_Admin.name}",
  ]
}

module "nikolayb" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "nikolayb"
  sbx_id    = "836196524082"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
    "${aws_iam_group.Endell_Admin.name}",
    "${aws_iam_group.Endell_Audit.name}",
    "${aws_iam_group.WilliamHill_ReadOnly.name}",
  ]
}

module "nikost" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "nikost"
  sbx_id    = "704830063211"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "ognyanm" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "ognyanm"
  sbx_id    = "235840456983"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
    "${aws_iam_group.Endell_Admin.name}",
    "${aws_iam_group.Endell_Audit.name}",
  ]
}

module "petars" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "petars"
  sbx_id    = "751555544831"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "radoslavy" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "radoslavy"
  sbx_id    = "389211405395"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
  ]
}

module "stanislavh" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "stanislavh"
  sbx_id    = "443360983586"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
    "${aws_iam_group.Finastra_Admin.name}",
    "${aws_iam_group.Tieto_Admin.name}",
    "${aws_iam_group.Tieto-Posti_Admin.name}",
    "${aws_iam_group.Traiana_Admin.name}",
  ]
}

module "stoyang" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "stoyang"
  sbx_id    = "335325095793"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

module "tihomirv" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "tihomirv"
  sbx_id    = "151101503030"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "valentinp" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "valentinp"
  sbx_id    = "968735802037"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
  ]
}

module "vasily" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "vasily"
  sbx_id    = "523520545767"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "vladimirs" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "vladimirs"
  sbx_id    = "707428059121"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

module "zhuliang" {
  source    = "../../../../lib/iam/user_with_sandbox"
  user_name = "zhuliang"
  sbx_id    = "973471407005"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.MS-Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto.name}",
    "${aws_iam_group.MS-Non_Tieto-2.name}",
  ]
}

resource "aws_iam_user" "brendanm" {
  name = "brendanm"
}

resource "aws_iam_user_group_membership" "brendanm" {
  user = "brendanm"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
  ]
}

resource "aws_iam_user" "deliand" {
  name = "deliand"
}

resource "aws_iam_user_group_membership" "deliand" {
  user = "deliand"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.BRB_Admin.name}",
  ]
}

resource "aws_iam_user" "dobromirt" {
  name = "dobromirt"
}

resource "aws_iam_user_group_membership" "dobromirt" {
  user = "dobromirt"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.HI_Admins.name}",
  ]
}

resource "aws_iam_user" "peterl" {
  name = "peterl"
}

resource "aws_iam_user_group_membership" "peterl" {
  user = "peterl"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}

resource "aws_iam_user" "steveb" {
  name = "steveb"
}

resource "aws_iam_user_group_membership" "steveb" {
  user = "steveb"

  groups = [
    "${aws_iam_group.HI_Users.name}",
    "${aws_iam_group.CGI_Admin.name}",
    "${aws_iam_group.WilliamHill_ReadOnly.name}",
  ]
}

resource "aws_iam_user" "yanitad" {
  name = "yanitad"
}

resource "aws_iam_user_group_membership" "yanitad" {
  user = "yanitad"

  groups = [
    "${aws_iam_group.HI_Users.name}",
  ]
}
