module "WilliamHill-1_ReadOnly" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "944611816277"
  policy_name = "WilliamHill-1_ReadOnly"
  role_name   = "helecloud-iam-readonly-ext"
}

module "WilliamHill-2_ReadOnly" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "344550086484"
  policy_name = "WilliamHill-2_ReadOnly"
  role_name   = "helecloud-iam-readonly-ext"
}

module "WilliamHill-3_ReadOnly" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "564606401287"
  policy_name = "WilliamHill-3_ReadOnly"
  role_name   = "helecloud-iam-readonly-ext"
}

resource "aws_iam_group" "WilliamHill_ReadOnly" {
  name = "WilliamHill_ReadOnly"
}

resource "aws_iam_group_policy_attachment" "WilliamHill-1_ReadOnly_WilliamHill_ReadOnly" {
  group      = "${aws_iam_group.WilliamHill_ReadOnly.name}"
  policy_arn = "${module.WilliamHill-1_ReadOnly.policy}"
}

resource "aws_iam_group_policy_attachment" "WilliamHill-2_ReadOnly_WilliamHill_ReadOnly" {
  group      = "${aws_iam_group.WilliamHill_ReadOnly.name}"
  policy_arn = "${module.WilliamHill-2_ReadOnly.policy}"
}

resource "aws_iam_group_policy_attachment" "WilliamHill-3_ReadOnly_WilliamHill_ReadOnly" {
  group      = "${aws_iam_group.WilliamHill_ReadOnly.name}"
  policy_arn = "${module.WilliamHill-3_ReadOnly.policy}"
}
