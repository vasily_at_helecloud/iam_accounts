locals {
  account_id = "891584517969"
}

resource "aws_iam_role" "helecloud_admin" {
  name               = "HeleCloud-Admin"
  assume_role_policy = "${file("${path.module}/files/trust_relationship.json")}"
}

resource "aws_iam_role_policy_attachment" "helecloud_admin-administratoraccess" {
  role       = "${aws_iam_role.helecloud_admin.name}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_role_policy_attachment" "helecloud_admin-billing" {
  role       = "${aws_iam_role.helecloud_admin.name}"
  policy_arn = "arn:aws:iam::aws:policy/job-function/Billing"
}

resource "aws_iam_role_policy_attachment" "helecloud_admin-infrasecmanage" {
  role       = "${aws_iam_role.helecloud_admin.name}"
  policy_arn = "arn:aws:iam::${local.account_id}:policy/InfraSecManage"
}

resource "aws_iam_policy" "update_accounts_html" {
  name   = "UpdateAccountsHtml"
  policy = "${file("${path.module}/files/update_accounts_html.json")}"
}

resource "aws_iam_role" "helecloud_readonly" {
  name               = "HeleCloud-ReadOnly"
  assume_role_policy = "${file("${path.module}/files/trust_relationship.json")}"
}

resource "aws_iam_role_policy_attachment" "helecloud_readonly-readonlyaccess" {
  role       = "${aws_iam_role.helecloud_readonly.name}"
  policy_arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}

resource "aws_iam_role_policy_attachment" "helecloud_readonly-update_accounts_html" {
  role       = "${aws_iam_role.helecloud_readonly.name}"
  policy_arn = "${aws_iam_policy.update_accounts_html.arn}"
}

module "HI_Admin" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "${local.account_id}"
  policy_name = "HI_Admin"
  role_name   = "${aws_iam_role.helecloud_admin.name}"
}

module "HI_ReadOnly" {
  source      = "../../../../lib/iam/account_role_policy"
  account_id  = "${local.account_id}"
  policy_name = "HI_ReadOnly"
  role_name   = "${aws_iam_role.helecloud_readonly.name}"
}

resource "aws_iam_policy" "manage_own_mfa" {
  name   = "ManageOwnMFA"
  policy = "${file("${path.module}/files/manage_own_mfa.json")}"
}

resource "aws_iam_policy" "manage_own_access_keys" {
  name   = "ManageOwnAccessKeys"
  policy = "${file("${path.module}/files/manage_own_access_keys.json")}"
}

resource "aws_iam_group" "HI_Admins" {
  name = "HI_Admins"
}

resource "aws_iam_group_policy_attachment" "HI_Admin-HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "${module.HI_Admin.policy}"
}

resource "aws_iam_group_policy_attachment" "iam_readonly_access-HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
}

resource "aws_iam_group_policy_attachment" "iam_user_ssh_keys-HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMUserSSHKeys"
}

resource "aws_iam_group_policy_attachment" "iam_user_change_password-HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMUserChangePassword"
}

resource "aws_iam_group_policy_attachment" "manage_own_mfa-HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "${aws_iam_policy.manage_own_mfa.arn}"
}

resource "aws_iam_group_policy_attachment" "manage_own_access_keys-HI_Admins" {
  group      = "${aws_iam_group.HI_Admins.name}"
  policy_arn = "${aws_iam_policy.manage_own_access_keys.arn}"
}

resource "aws_iam_group" "HI_Users" {
  name = "HI_Users"
}

resource "aws_iam_group_policy_attachment" "HI_ReadOnly-HI_Users" {
  group      = "${aws_iam_group.HI_Users.name}"
  policy_arn = "${module.HI_ReadOnly.policy}"
}

resource "aws_iam_group_policy_attachment" "iam_readonly_access-HI_Users" {
  group      = "${aws_iam_group.HI_Users.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
}

resource "aws_iam_group_policy_attachment" "iam_user_ssh_keys-HI_Users" {
  group      = "${aws_iam_group.HI_Users.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMUserSSHKeys"
}

resource "aws_iam_group_policy_attachment" "iam_user_change_password-HI_Users" {
  group      = "${aws_iam_group.HI_Users.name}"
  policy_arn = "arn:aws:iam::aws:policy/IAMUserChangePassword"
}

resource "aws_iam_group_policy_attachment" "manage_own_mfa-HI_Users" {
  group      = "${aws_iam_group.HI_Users.name}"
  policy_arn = "${aws_iam_policy.manage_own_mfa.arn}"
}

resource "aws_iam_group_policy_attachment" "manage_own_access_keys-HI_Users" {
  group      = "${aws_iam_group.HI_Users.name}"
  policy_arn = "${aws_iam_policy.manage_own_access_keys.arn}"
}
