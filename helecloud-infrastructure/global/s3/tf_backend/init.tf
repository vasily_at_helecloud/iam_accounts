terraform {
  required_version = "= 0.11.8"

  backend "local" {
    path = "state/tf_backend.local"
  }
}

provider "aws" {
  region = "${var.region}"
}
