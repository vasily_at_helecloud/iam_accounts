data "aws_iam_account_alias" "current" {}

locals {
  account_alias = "${data.aws_iam_account_alias.current.account_alias}"
}

module "tf_backend-s3" {
  source         = "../../../../lib/s3/tf_backend"
  region         = "${var.region}"
  bucket_name    = "${local.account_alias}-terraform-state"
  tag-state_path = "global/s3"
  tag-tf_module  = "tf_backend"
}
