output "bucket_name" {
  value = "${module.tf_backend-s3.bucket_name}"
}

output "table_name" {
  value = "${module.tf_backend-s3.table_name}"
}
