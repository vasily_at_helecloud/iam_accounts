# iam_accounts

In `lib/` are the modules which should be called by ___other___ modules and not store state themselves.

Each module has a `README.md` file which explains their function. Refer to them for details.

## Directory structure
```
.
├── helecloud-infrastructure
│   └── global
│       ├── iam
│       │   └── accounts
│       │       ├── client_Assystem.tf
│       │       ├── client_BRB.tf
│       │       ├── client_CGI.tf
│       │       ├── client_Endell.tf
│       │       ├── client_Finastra.tf
│       │       ├── client_NetSend.tf
│       │       ├── client_Three-Mashery.tf
│       │       ├── client_Three.tf
│       │       ├── client_Tieto-Posti.tf
│       │       ├── client_Tieto.tf
│       │       ├── client_Traiana.tf
│       │       ├── client_WilliamHill.tf
│       │       ├── client_YouView.tf
│       │       ├── client_Zopa.tf
│       │       ├── files
│       │       │   ├── manage_own_access_keys.json
│       │       │   ├── manage_own_mfa.json
│       │       │   ├── trust_relationship.json
│       │       │   └── update_accounts_html.json
│       │       ├── group_MS-Non_Tieto-2.tf
│       │       ├── group_MS-Non_Tieto.tf
│       │       ├── group_MS-Tieto.tf
│       │       ├── helecloud_infrastructure.tf
│       │       ├── hi_internal_accounts.tf
│       │       ├── init.tf
│       │       ├── outputs.tf
│       │       ├── README.md
│       │       ├── users.tf
│       │       └── variables.tf
│       └── s3
│           └── tf_backend
│               ├── init.tf
│               ├── outputs.tf
│               ├── state
│               │   └── tf_backend.local
│               ├── tf_backend.tf
│               └── variables.tf
├── lib
│   ├── iam
│   │   ├── account_role_policy
│   │   │   ├── account_role_policy.tf
│   │   │   ├── init.tf
│   │   │   ├── outputs.tf
│   │   │   ├── README.md
│   │   │   ├── templates
│   │   │   │   └── account_role_policy.json
│   │   │   └── variables.tf
│   │   └── user_with_sandbox
│   │       ├── init.tf
│   │       ├── README.md
│   │       ├── user_with_sandbox.tf
│   │       └── variables.tf
│   └── s3
│       └── tf_backend
│           ├── dynamodb_table.tf
│           ├── init.tf
│           ├── outputs.tf
│           ├── README.md
│           ├── s3_bucket.tf
│           └── variables.tf
└── README.md

15 directories, 50 files
```
